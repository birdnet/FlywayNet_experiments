########## MUSE


rm slurm-*.out ; rm log/jobO*.Rout ; sbatch job_muse.sub


## If you need synchonistation between save dir and local
# rsync -e ssh -varz treposr@muse-login.hpc-lr.univ-montp2.fr:/lustre/treposr/FlywayNet_experiments/curlew/rdata /pub/src/birdnet_forgemia/FlywayNet_experiments/benchmark/
# rsync -e ssh -varz /pub/src/birdnet_forgemia/FlywayNet_experiments/benchmark/rdata treposr@muse-login.hpc-lr.univ-montp2.fr:/lustre/treposr/FlywayNet_experiments/benchmark/

########## MIAT SERVER

## If you need synchonistation between MIAT server and local

rsync -e ssh -varz rtrepos@147.99.96.17:/home/rtrepos/FlywayNet_experiments/benchmark/rdata /pub/src/birdnet_forgemia/FlywayNet_experiments/benchmark
