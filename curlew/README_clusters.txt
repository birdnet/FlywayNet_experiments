########## MUSE


rm slurm-*.out ; rm log/jobO*.Rout ; sbatch job_muse.sub


## If you need synchonistation between work and save directories
rsync rdata/*rdata /home/treposr/birdnet_save/experiments/curlew/rdata

## If you need synchonistation between save dir and local
# rsync -e ssh -varz treposr@muse-login.hpc-lr.univ-montp2.fr:/nfs/home/treposr/birdnet_save/experiments/curlew/rdata /pub/src/birdnet_forgemia/FlywayNet_experiments/curlew/
# rsync -e ssh -varz /pub/src/birdnet_forgemia/FlywayNet_experiments/curlew/rdata treposr@muse-login.hpc-lr.univ-montp2.fr:/nfs/home/treposr/birdnet_save/experiments/curlew/

########## MIAT SERVER

## If you need synchonistation between MIAT server and local

rsync -e ssh -varz rtrepos@147.99.96.17:/home/rtrepos/birdnet/experiments/curlew/rdata /pub/src/birdnet_forgemia/FlywayNet_experiments/curlew/
