rm(list=ls())
source("../utils/utils_plot.R")
source("../utils/utils_expe_post.R")
library(FlywayNet)


figs_par = list(
  figs_dir = "figures",
  figs_name ="uncertainty_p_curlew",
  plots = c("posterior", "evolMCEM", "loglikelihood"),
  factor_p_domain =  c(0.1, 1, 1e1, 1e2, 1e3)
)



all_res = NULL
year = 2018
obs_law = "NegBin_p"
for (factor_p in figs_par$factor_p_domain) {
  
  rr = get_rdata_files_curlew_uncertainty_p(factor_p)
  
  if ("posterior" %in% figs_par$plots) {
    file1 = paste(sep="",figs_par$figs_dir, "/", figs_par$figs_name, "_", year,
                  "_", factor_p, "_posterior.jpeg")
    all_res = add_to_df(all_res, extract_experiment(
      rr, select_ABClike= FALSE, true_values_known=F, plotted=file1))
  }
  
  if ("posteriorABClike" %in% figs_par$plots) {
    file1 = paste(sep="",figs_par$figs_dir, "/", figs_par$figs_name, "_", year,
                  "_", factor_p, "_posteriorABClike.jpeg")
    all_res = add_to_df(all_res, extract_experiment(
      rr, select_ABClike= TRUE, true_values_known=F, plotted=file1))
  }
  
  
  if ("evolMCEM" %in% figs_par$plots) {
    file2 = paste(sep="",figs_par$figs_dir, "/", figs_par$figs_name, "_", year,
                  "_", factor_p, "_evolMCEM.jpeg")
    res_MCEM = extract_mcem_evol_params(rr, true_values_known = F, file2)
  }
  
  if ("loglikelihood" %in% figs_par$plots) {
    load(paste(sep="",  paste(sep="", "rdata_post/uncertainty_p_curlew_", year,
                              "_", factor_p, ".rdata"))) #load loglikelihood
    file3 = paste(sep="",figs_par$figs_dir, "/", figs_par$figs_name, "_", year,
                  "_", factor_p, "_loglikelihood.jpeg")
    jpeg(file3, width=1200, height=400)
    plot(ggplot(stack(as.data.frame(loglikelihood)), aes(x = ind, y = values)) +
           geom_boxplot())
    dev.off()
  }
}



