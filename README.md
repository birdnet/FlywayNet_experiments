---
output:
  pdf_document: default
  html_document: default
---
# R code and execution results of the article 'FlywayNet: A hidden semi-Markov model for inferring the structure of migratory bird flyway networks'

The article relies on FlywayNet R package available via an [INRAE gitlab](https://forgemia.inra.fr/birdnet/FlywayNet). To install it on a computer here are the R commands:
```
   > install.packages('remotes')
   > remotes::install_gitlab(repo="birdnet/FlywayNet", host="https://forgemia.inra.fr/")
```
   
To re-generate results, load the archive FlywayNet_experiments.zip on [FigShare](https://doi.org/10.6084/m9.figshare.16658185.v5).

Extract files from this archive and go to the main directory [^1]:
```
   # unzip FlywayNet_experiments.zip
   # cd FlywayNet_experiments
```

Here is the list of R used packages: dplyr, GGallin, ggallin, ggplot2, ggpubr, grid, gridExtra, igraph, MASS, pander, plyr, reshape2, rstatix, tictoc. Note that all are not required to re-run part of the archive.


## To re-run the notebooks of the curlew case study

Re-run the notebooks for the two considered years

  * Go to final_scripts sub-directory and call Rstudio.

  * If necessary, install dplyr, bayestestR, ggplot2 and knitr packages.
  
  * Open curlew_2018.Rmd file, then click on 'Knit' button.
  
  * Open curlew_2019.Rmd file, then click on 'Knit' button.
  
  The files curlew_2018.pdf and curlew_2019.pdf are then created.


## To re-run the experiments 

The results of experiments are stored in RData files in the following directories:

  * benchmark/rdata, benchmark/rdata_post, benchmark/rdata_comp for benchmark results,
  
  * curlew/rdata, curlew/rdata_post for the curlew case study.

Just remote the RData files you want to re-generate.

a/ For example, to re-run ABC method for the curlew case study, year 2018:
```
  # rm curlew/rdata/curlew_2018_NegBin_p_ABC.rdata
```

If necessary, install required packages: igraph, tictoc, MASS, dplyr.

Then go to the curlew sub-directory and run the following command:
```
  # R CMD BATCH --no-save --no-restore  expe_curlew.R
```
Note that the file expe_curlew.Rout allows to read the log of the expe_curlew.R execution.

b/ To re-run part of the benchmark, the same way, delete a case, for example:
```
  # rm benchmark/rdata_post/benchmark_NegBin_p_4_2_0_0.rdata
```
  
If necessary, install required packages: igraph, tictoc, MASS, pander, GGally, gridExtra.

Then go to the benchmark sub-directory and run the following command:
```
  # R CMD BATCH --no-save --no-restore  benchmark_post.R
```
Note that the file expe_curlew.Rout allows to read the log of the expe_curlew.R execution.


##  Description of the directories

### Benchmark experiments (directory benchmark)

- Script benchmark.R: launch all the benchmark experiments. Results are rdata files in the rdata directory.
WARNING: it can very long to run (more than 1 week) if no specific computer ressources are provided.
Runs can be distributed on mutliple cores based on the utils functions in ../utils/utils_mutex.R.
Only a subset of runs can be launched by changing the parameters.

- Script benchmark_post.R: post process results to provide estimation of the loglikelihood (by simulation) 
of estimated parameters. Results are rdata files in rdata_post directory.

- Script benchmark_figures.R: produce figures to analyse results of individual experiments. 
Results are jpeg files in directory figures.

- Script benchmark_compile.R: produce a summary of all the benchmark experiments. 
Results are rdata files in the rdata_comp directory.

 
### Far eastern curlew experiments (directory curlew)

- Script expe_curlew.R: launch all the curlew experiments. Results are rdata files in the rdata directory.
WARNING: it can very long to run if no specific computer ressources are provided.
Runs can be distributed on mutliple cores based on the utils functions in ../utils/utils_mutex.R.
Only a subset of runs can be launched by changing the parameters.

- Script expe_curlew_post.R: post process results to provide estimation of the loglikelihood (by simulation) 
of estimated parameters. Results are rdata files in rdata_post directory.

- Script expe_curlew_figures.R: produce figures to analyse results of individual experiments. 
Results are jpeg files in directory figures.

- Script curlew_utils.R: Specific function for the curlew experiment. It essentially builds the initial 
migration structures for years 2018 and 2019.


### Directory utils 

Utils directory contains R functions used for both benchmark and curlew experiments.


### Directory final_scripts 

The directory final_scripts contains R and Rmd files that process the figures for the 
manuscript.


## Original execution configuration

The FlywayNet package version is tagged by "V1.0" on gitlab.

The R environment is the following:
```
  >  print(sessionInfo())
R version 4.2.0 (2022-04-22)
Platform: x86_64-pc-linux-gnu (64-bit)
Running under: Ubuntu 20.04.4 LTS

Matrix products: default
BLAS:   /usr/lib/x86_64-linux-gnu/blas/libblas.so.3.9.0
LAPACK: /usr/lib/x86_64-linux-gnu/lapack/liblapack.so.3.9.0

locale:
  [1] LC_CTYPE=fr_FR.UTF-8       LC_NUMERIC=C
  [3] LC_TIME=fr_FR.UTF-8        LC_COLLATE=fr_FR.UTF-8
  [5] LC_MONETARY=fr_FR.UTF-8    LC_MESSAGES=fr_FR.UTF-8
  [7] LC_PAPER=fr_FR.UTF-8       LC_NAME=C
  [9] LC_ADDRESS=C               LC_TELEPHONE=C
[11] LC_MEASUREMENT=fr_FR.UTF-8 LC_IDENTIFICATION=C

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods base

other attached packages:
  [1] dplyr_1.0.7        cowplot_1.1.1      scales_1.1.1 RColorBrewer_1.1-2
  [5] plyr_1.8.6         reshape2_1.4.4     gridExtra_2.3 rstatix_0.7.0
  [9] ggpubr_0.4.0       igraph_1.3.0       ggplot2_3.3.5 MASS_7.3-56
[13] FlywayNet_0.1.1    pander_0.6.4

loaded via a namespace (and not attached):
  [1] tidyselect_1.1.1  purrr_0.3.4       haven_2.4.1 carData_3.0-4
  [5] colorspace_2.0-2  vctrs_0.3.8       generics_0.1.0 utf8_1.2.1
  [9] rlang_0.4.11      pillar_1.6.1      foreign_0.8-82 glue_1.4.2
[13] withr_2.4.2       readxl_1.3.1      lifecycle_1.0.0 stringr_1.4.0
[17] munsell_0.5.0     ggsignif_0.6.2    gtable_0.3.0 cellranger_1.1.0
[21] zip_2.2.0         rio_0.5.27        forcats_0.5.1 curl_4.3.2
[25] fansi_0.5.0       broom_0.7.8       Rcpp_1.0.8.3 backports_1.2.1
[29] abind_1.4-5       hms_1.1.0         digest_0.6.27 stringi_1.6.2
[33] openxlsx_4.2.4    grid_4.2.0        tools_4.2.0 magrittr_2.0.3
[37] tibble_3.1.2      crayon_1.4.1      car_3.0-11 tidyr_1.1.3
[41] pkgconfig_2.0.3   ellipsis_0.3.2    data.table_1.14.0 R6_2.5.0
[45] compiler_4.2.0
```


[^1]: lines beginning with '#' are Unix command lines ; lines beginning with '<' are R command lines
